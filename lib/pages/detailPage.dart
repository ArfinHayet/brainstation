import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class ScreenArguments {
  final String name;
  final String full_name;
  final String description;
  final String avatar_url;
  final String updated_at;
  final String stargazers_count;
  final String html_url;
  final String topics;
  final bool isOnline;
  ScreenArguments(this.name,this.full_name,this.description,this.avatar_url,this.updated_at,this.stargazers_count,this.html_url,this.topics,this.isOnline);
}

class Detail extends StatefulWidget {
  const Detail({Key? key}) : super(key: key);

  @override
  State<Detail> createState() => _DetailState();
}

class _DetailState extends State<Detail> {





  String name = '';
  String description = '';
  String avatar_url = '';
  String updated_at = '';
  String stargazers_count = '';
  String html_url = '';
  String topics = '';
  String full_name = '';
  bool isOnline = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    final args = ModalRoute.of(context)!.settings.arguments as  Map<String, dynamic>;

    setState(() {
      name = args['name'];
      description = args['description'];
      avatar_url = args['avatar_url'];
      updated_at = args['updated_at'];
      stargazers_count = args['stargazers_count'];
      html_url = args['html_url'];
      topics = args['topics'];
      isOnline = args['isOnline'];
      full_name = args['full_name'];
    });

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text(full_name,style: TextStyle(color: Colors.black),),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width*1,
            child: Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    isOnline ? Container(
                      width: 150.0,
                      height: 150.0,
                      decoration: BoxDecoration(
                        color: const Color(0xff7c94b6),
                        image: DecorationImage(
                          image: NetworkImage(avatar_url),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.all( Radius.circular(150.0)),
                        border: Border.all(
                          color: Colors.red,
                          width: 4.0,
                        ),
                      ),
                    ) : SizedBox(),
                    Text(name,style: TextStyle(fontSize: 30),),
                    Divider(),
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(7),
                  margin:EdgeInsets.all(3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.drive_file_rename_outline),
                          Text('Project Name'),
                        ],
                      ),
                      Text(name,style: TextStyle(
                          color: Colors.black45
                      ),),
                    ],
                  ),
                ),
                Divider(),
                Container(
                  padding: EdgeInsets.all(7),
                  margin:EdgeInsets.all(3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.description),
                          Text('Description'),
                        ],
                      ),
                      Text(description,style: TextStyle(
                        color: Colors.black45
                      ),),
                    ],
                  ),
                ),
                Divider(),

                Container(
                  padding: EdgeInsets.all(7),
                  margin:EdgeInsets.all(3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.update),
                          Text('Last Update'),
                        ],
                      ),

                      Text(DateFormat('yMd').add_jm().format(DateTime.parse(updated_at)).toString(),style: TextStyle(
                          color: Colors.black45
                      ),),
                    ],
                  ),
                ),
                Divider(),

                Container(
                  padding: EdgeInsets.all(7),
                  margin:EdgeInsets.all(3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.star),
                          Text('Total Liked'),
                        ],
                      ),

                      Text(stargazers_count,style: TextStyle(
                          color: Colors.black45
                      ),),
                    ],
                  ),
                ),
                Divider(),

                Container(
                  padding: EdgeInsets.all(7),
                  margin:EdgeInsets.all(3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.web),
                          Text('Web Url'),
                        ],
                      ),

                      GestureDetector(
                        onTap: (){
                          launchUrl(Uri.parse(html_url));
                        },
                        child: Text(html_url,style: TextStyle(
                            color: Colors.blue
                        ),),
                      ),
                    ],
                  ),
                ),
                Divider(),

                Container(
                  padding: EdgeInsets.all(7),
                  margin:EdgeInsets.all(3),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.topic),
                          Text('Topics'),
                        ],
                      ),

                      Text(topics,style: TextStyle(
                          color: Colors.black45
                      ),),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
