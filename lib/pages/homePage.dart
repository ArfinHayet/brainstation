import 'dart:convert';
import 'package:brainstation/services/db_table.dart';
import 'package:http/http.dart' as http;
import 'package:brainstation/services/fetchData.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List repos = [];
  List filters = [
    {'option':'Most Stars','value':'stars','order':'desc'},
    {'option':'Fewest Stars','value':'stars','order':'asc'},
    {'option':'Recently Updated','value':'updated','order':'desc'},
    {'option':'Least Recently Updated','value':'updated','order':'asc'},
  ];
  int page = 1;
  bool isLoadingMore = false;
  bool isOnline = false;
  var sortBy = 'desc';
  var sort = 'stars';
  String keyword = 'flutter';
  final scrollController = ScrollController();



  showAlert(msg){
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Okay',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future<void> getRepodata() async {
    final response = await http
        .get(Uri.parse('https://api.github.com/search/repositories?q=${keyword}&page=${page}&per_page=10&sort=${sort}&order=${sortBy}'));
    if (response.statusCode == 200) {

      var data = jsonDecode(response.body);
      var finalData = data['items'];

      setState(() {
        repos = repos + finalData;
        isLoadingMore = false;
      });

      for(var i=0;i<finalData.length;i++){
        await DbHelper.createItem(finalData[i]['name'],finalData[i]['full_name'],finalData[i]['description'],finalData[i]['owner']['avatar_url'],finalData[i]['updated_at'],finalData[i]['stargazers_count'].toString(),finalData[i]['html_url'],finalData[i]['topics'].toString());
      }

    }
  }

  Future<void> scrollListener() async{
    if(scrollController.position.pixels == scrollController.position.maxScrollExtent){

      setState(() {
        isLoadingMore = true;
      });
      page = page + 1;
      await getRepodata();
    } else {
    }
  }

  void _offlineData() async{
    final data = await  DbHelper.getRepos();
    setState(() {
      repos = data;
    });
  }

  void internetCheck() async{
    bool result = await InternetConnectionChecker().hasConnection;
    setState((){
      isOnline = result;
    });

    if(isOnline){
      DbHelper.delRepo();
      getRepodata();
      showAlert('Browsing in online mode');
    } else {
      _offlineData();
      showAlert('Browsing in offline mode');
    }
}

  @override
  void initState(){
    internetCheck();
    scrollController.addListener(scrollListener);
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Flutter Repos',style: TextStyle(color: Colors.black),),
            GestureDetector(
                onTap: (){
                  showModalBottomSheet<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        height: 250,
                        color: Colors.white,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                               Icon(Icons.keyboard_arrow_down_sharp),
                               isOnline ? Expanded(
                                 child: ListView.builder(
                                 itemCount : filters.length,
                                 itemBuilder: (context, index) {
                                    return GestureDetector(
                                      onTap: (){
                                        setState(() {
                                          sort = filters[index]['value'];
                                          sortBy = filters[index]['order'];
                                          repos = [];
                                          getRepodata();
                                        });
                                        Navigator.pop(context);
                                      },
                                      child: Container(
                                        color: filters[index]['value'].compareTo(sort) == 0 && filters[index]['order'].compareTo(sortBy) == 0 ? Color(0xffdddddd) : Colors.white,
                                        child: ListTile(
                                          leading: Icon(Icons.sort),
                                          title: Text(filters[index]['option']),
                                        ),
                                      )
                                    );
                                 }),
                               ) : Container(
                                 child: Text('Sorting is not avaiable in ofline mode'),
                               )
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
                child: Icon(Icons.sort)),
          ],
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SafeArea(
        child:  repos.length == 0 ? Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            isOnline ?
            CupertinoActivityIndicator(
              radius: 15,
            ) :
                Container(
                  margin: EdgeInsets.fromLTRB(0,30,0,0),
                  child: Column(
                    children: [
                      Text('Turn on internet to save offline'),
                      ElevatedButton(onPressed: (){
                         setState(() {
                           repos = [];
                           internetCheck();
                         });
                      }, child: Text('Retry'))
                    ],
                  ),
                )
          ],
        ) :
          ListView.builder(
            controller: isOnline ? scrollController : null,
            itemCount: isLoadingMore ? repos.length + 1 : repos.length,
            itemBuilder: (context,index){

              if(index<repos.length){
                return GestureDetector(
                  onTap: (){
                    Navigator.pushNamed(context, '/detail',arguments: {
                      'name' : repos[index]['name'],
                      'full_name' : repos[index]['full_name'],
                      'description' : repos[index]['description'].toString(),
                      'avatar_url' : isOnline ? repos[index]['owner']['avatar_url'].toString() : repos[index]['avatar_url'].toString(),
                      'updated_at' : repos[index]['updated_at'].toString(),
                      'stargazers_count' : repos[index]['stargazers_count'].toString(),
                      'html_url' : repos[index]['html_url'].toString(),
                      'topics' : repos[index]['topics'].toString(),
                      'isOnline' : isOnline,

                    });
                  },
                  child: ListTile(
                    leading: CircleAvatar(
                      child: Icon(Icons.list),
                    ),
                    title: Text(repos[index]['full_name']),
                    subtitle: Text(DateFormat('yMd').add_jm().format(DateTime.parse(repos[index]['updated_at'].toString())).toString()),
                    trailing: Text('⭐'+repos[index]['stargazers_count'].toString()),
                  ),
                );
              } else { 
                return CupertinoActivityIndicator(
                  radius: 15,
                );
              }

        })
      ),
    );
  }
}
