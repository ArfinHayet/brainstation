import 'package:flutter/material.dart';
import 'package:brainstation/pages/homePage.dart';
import 'package:brainstation/pages/detailPage.dart';


void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/' : (context) => Home(),
      '/detail' : (context) => Detail()
    },
  ));
}
