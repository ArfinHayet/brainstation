import 'dart:convert';
import 'package:http/http.dart' as http;

class Getrepo{

  Future<List<Repo>> getRepodata() async {
    String keyword = 'flutter';
    final response = await http
        .get(Uri.parse('https://api.github.com/search/repositories?q=${keyword}'));
    if (response.statusCode == 200) {

      var data = jsonDecode(response.body);
      var finalData = data['items'];
      List<Repo> repos = [];

      for(var u in finalData){
        Repo repo = Repo(id: u['id'].toString(),name:u['name'].toString(), full_name: u['full_name'], updated_at: u['updated_at'].toString(), stargazers_count: u['stargazers_count'].toString(), avatar_url: u['owner']['avatar_url'].toString(),description: u['description'].toString());
        repos.add(repo);
      }

      return repos;

    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load repos');
    }
  }
}

class Repo {
  final String id;
  final String name;
  final String full_name;
  final String updated_at;
  final String stargazers_count;
  final String avatar_url;
  final String description;


  Repo({
    required this.id,
    required this.name,
    required this.full_name,
    required this.updated_at,
    required this.stargazers_count,
    required this.avatar_url,
    required this.description
  });
}