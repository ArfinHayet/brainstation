import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;
class DbHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE repos(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        name TEXT,
        full_name TEXT,
        description TEXT,
        avatar_url Text,
        updated_at Text,
        stargazers_count INT,
        html_url TEXT,
        topics TEXT
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }


  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'reposetory.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  static Future<int> createItem(String name,String full_name, String? descrption, String? avatar_url,String? updated_at,String? stargazers_count,String? html_url, String? topics) async {
    final db = await DbHelper.db();

    final data = {'name':name,'full_name':full_name,'description':descrption,'avatar_url':avatar_url,'updated_at':updated_at,'stargazers_count':stargazers_count,'html_url':html_url,'topics':topics};
    final id = await db.insert('repos', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }


  static Future<List<Map<String, dynamic>>> getRepos() async {
    final db = await DbHelper.db();
    return db.query('repos', orderBy: "id DESC");
  }


  static Future<void> delRepo() async {
    final db = await DbHelper.db();
    try {
      await db.delete("repos");
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
  }



}